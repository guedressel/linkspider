from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.item import Item, Field


class LinkItem(Item):
    url = Field()  # url that was requested
    status = Field()  # status code received
    referrer = Field()  # where the link is extracted


class Linkspider(CrawlSpider):

    name = "linkspider"
    target_domains = ["www.cyledge.com"]
    start_urls = ["https://www.cyledge.com/"]
    url_pattern_deny = ()
    handle_httpstatus_all = True
    custom_settings = {
        'CONCURRENT_REQUESTS': 2,  # only 2 requests at the same time
        'DOWNLOAD_DELAY': 0.5,  # delay between requests
        'BOT_NAME': 'CYLEDGE linkspider'
    }

    rules = [
        # crawl internal links
        Rule(
            LinkExtractor(allow_domains=target_domains, deny=url_pattern_deny, unique=('Yes')), callback='parse',
            follow=True, process_links='filter_links'),
        # crawl external links but don't follow them
        Rule(
            LinkExtractor(allow=(''), deny=url_pattern_deny, unique=('Yes')), callback='parse', follow=False,
            process_links='filter_links')
    ]

    # Filter links with the nofollow attribute
    def filter_links(self, links):
        filtered_links = []
        if links:
            for link in links:
                if not link.nofollow:
                    filtered_links.append(link)
                else:
                    self.logger.debug('Dropped link %s because nofollow attribute was set.' % link.url)
        return filtered_links

    def parse(self, response, **kwargs):
        item = LinkItem()
        item['url'] = response.url
        item['status'] = response.status
        if 'Referer' in response.request.headers:
            item['referrer'] = response.request.headers.get('Referer').decode('UTF-8')
        else:
            item['referrer'] = None
        yield item
